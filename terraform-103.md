# 103 - Provisioning

## Lab

Er is een optie `provisioner` binnen de resource type die gebruikt kan worden.
Zoals de documentatie ook beschrijft bestaat de mogelijkheid om verschillende provisioning types te gebruiken:

- remote-exec
- file
- chef
- habitat
- local-exec
- puppet
- salt-masterless

Voor deze opdracht kunnen we af met _remote-exec_.

### Opdracht 3 - Installeer een webserver

Nu dat een instance is aangemaakt willen we een webserver gaan gebruiken.
In de [Provisioners overzicht](https://www.terraform.io/docs/provisioners) kunnen vinden welke provisioners mogelijk zijn.

Voor het installeren van een webserver kunnen we de volgende commando's gebruiken:

```bash
sudo apt-get update
sudo apt-get install -y apache2
hostname -I | sudo tee /var/www/html/index.html
```

### Vragen

- Waarom zien we geen veranderingen als we een `plan` uitvoeren?
- Hoe kunnen we toch dit laten uitvoeren?
- Waarom gaat het provisioning fout?
- Als we de instance willen behouden, hoe kunnen we dan alsnog provisioning uitvoeren?
