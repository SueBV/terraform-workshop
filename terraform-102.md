# 102 - SSH public key

## Lab

### Opdracht 2: Koppel een SSH public key

Upload een SSH public key naar AWS en koppel deze aan de instance.

Een handige pagina om ernaast te hebben is [AWS overzicht](https://www.terraform.io/docs/providers/aws/) op de [Terraform website](https://terraform.io).

Zoals we gemerkt hebben kunnen we niet inloggen op de aangemaakte instance, omdat er geen SSH key gekoppeld is. Gelukkig is hier ook rekening mee gehouden.
Binnen de resource kan de optie `key_name` meegegeven worden, alleen moet eerst de public SSH key aangemaakt zijn binnen AWS.

### Vragen

- Hoe kunnen we de aangemaakte resource koppelen aan de `key_name`
- Welke andere manier is er nog om de public key te koppelen?

