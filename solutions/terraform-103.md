# 103 - Provisioning

Binnen *aws_instance* kan de provisioner gedefineerd worden. Voor deze opdracht voldoet *remote-exec* type prima.

```hcl
provisioner "remote-exec" {
  inline = [
    "sudo apt-get update",
    "sudo apt-get install -y apache2",
    "hostname -I | sudo tee /var/www/html/index.html"
  ]

  connection {
    user        = "ubuntu"
    host        = self.public_ip
    timeout     = "5m"
  }

}
```

Andere optie is, is gebruiken van een `null_resource` resource. Voor meer informatie zie [null_resource](https://www.terraform.io/docs/provisioners/null_resource.html) documentatie.

### Antwoorden

- Waarom zien we geen veranderingen als we een `plan` uitvoeren?
  *Omdat de instance al is aangemaakt en provisioner geen onderdeel is van het wijzigen van de instance binnen AWS, alleen in het Operating System.*
- Hoe kunnen we toch dit laten uitvoeren?
  *Er zijn twee mogelijkheden, we geven aan dat de resource vervuild is (tainted, red.) of maken gebruik van een null_resource type.*
- Waarom gaat het provisioning fout?
  *Standaard kijkt terraform niet naar je `~/.ssh/id_rsa` als private key, dus deze moet dan gedefineerd worden of via SSH agent.*
- Als we de instance willen behouden, hoe kunnen we dan alsnog provisioning uitvoeren?
  *Dit kan via een null_resource type*
