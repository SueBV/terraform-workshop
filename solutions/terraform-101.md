# 101 - Instance

Met *aws_instance* resource type is het mogelijk om één of meerdere instances aan te maken.

```hcl
resource "aws_instance" "web" {
  ami           = var.ami-image
  instance_type = "t2.micro"

  tags = {
    Name = "web-${random_pet.rand.id}"
  }
}
```
