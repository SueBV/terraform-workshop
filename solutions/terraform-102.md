# 102 - SSH public key

Binnen de `aws_instance` resource type moet de naam opgegeven worden van de key. Als er al een key bestaat binnen AWS is het mogelijk om deze op te geven. Andere optie is om deze aan te maken.

```hcl
resource "aws_key_pair" "ikke" {
  key_name   = "mijn-key"
  public_key = "ssh-rsa AAAA..."
}
```

Andere optie is om gebruik te maken van een `file` functie.

```hcl
resource "aws_key_pair" "ikke" {
  key_name   = "mijn-key"
  public_key = file("/home/robin/.ssh/id_rsa.pub")
}
```

Daarna kan de key gekoppeld worden aan een instance:

```hcl
resource "aws_instance" "web" {
  ami           = var.ami-image
  instance_type = "t2.micro"
  key_name      = aws_key_pair.ikke.id

  tags = {
    Name = "web-${random_pet.rand.id}"
  }
}
```
