# 102 - Multiply

Aan de `aws_instance` resource type is het mogelijk om een `count` optie mee te geven.
Als deze wordt toegevoegd en uitgevoerd dan krijgen we een error. Er bestaat al een instance met deze naam en kan dus niet aangemaakt worden.

Omdat `count` gebruikt wordt kunnen we de `count.index` raadplegen om zo unieke namen mee te geven.

```hcl
resource "aws_instance" "web" {
  count         = 3
  ami           = var.ami-image
  instance_type = "t2.micro"
  key_name      = aws_key_pair.ikke.id

  tags = {
    Name = "web-${random_pet.rand.id}-${count.index}"
  }
}
```

Of als we niet bij nul willen beginnen:

```hcl
resource "aws_instance" "web" {
  count         = 3
  ami           = var.ami-image
  instance_type = "t2.micro"
  key_name      = aws_key_pair.ikke.id

  tags = {
    Name = format("web-${random_pet.rand.id}-%02d", count.index + 1)
  }
}
```

## Antwoorden

- Welk probleem krijgen we al meer dan 1 instance aanmaken?
  *Dit komt omdat de naam niet uniek is. Door middel van een count.index mee te geven krijgen ze een unieke naam.*
- Hoe kunnen we dit hetbeste oplossen?
  *Door middel van count.index te gebruiken.*
