# 105 - Loadbalancing

Om de beschikbare availability zones te gebruiken voor je resources kan je met `aws_availability_zones` data deze bij AWS opvragen.

```hcl
data "aws_availability_zones" "all" {}
```

Om een _ELB Classic_ aan te maken gebruiken we de *aws_elb* resource type:

```hcl
resource "aws_elb" "tf" {
  name               = "tf101-${random_pet.rand.id}"
  availability_zones = data.aws_availability_zones.all.names

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 30
    target              = "HTTP:80/"
  }

  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = "80"
    instance_protocol = "http"
  }

  instances = aws_instance.web.*.id
}
```
