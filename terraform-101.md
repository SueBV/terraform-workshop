# 101 - Instance

## Voorbereiding

Voor de oefeningen gaan we gebruik maken van AWS. Om te beginnen hebben we een AWS Access ID en AWS Secret Key nodig. (*AWS ACCESS ID* en *AWS SECRET KEY*)
Als een cursist zelf al AWS credentials heeft kan/mag deze gebruikt worden.
Het is verstandig om de AWS credentials niet in de bestanden op te nemen, maar als variables.

```
export AWS AWS_ACCESS_KEY_ID=AKIA...
export AWS_SECRET_ACCESS_KEY=...
```

## Commando's

Zoals al aangegeven in de presentatie zijn er aantal Terraform commando's die we kunnen gebruiken.

- `terraform init`
- `terraform plan`
- `terraform apply`
- `terraform destroy`
- `terraform show`
- `terraform plan -destroy`

## Lab

### Opdracht 1: Crëeer een instance

Maak een _t2.micro_ instance aan in AWS met Ubuntu 18.04 image en ga na wat het IP adres is van deze instance.

Om een beetje in de goede richting te helpen staan hieronder Terraform configuratie die we nodig hebben voordat we een instance in AWS kunnen aanmaken.

```hcl
provider "aws" {
  region = "eu-west-1"
}

variable "ami-image" {
  type    = string
  default = "ami-01cca82393e531118"
}

resource "random_pet" "rand" {}
```

Een handige pagina om ernaast te hebben is [AWS instance](https://www.terraform.io/docs/providers/aws/r/instance.html) op de [Terraform website](https://terraform.io).

