# Terraform workshop 101

## Basis informatie

Voor de Terraform workshop wordt er gebruik gemaakt van AWS. Om te beginnen hebben we een AWS Access Key nodig. (*AWS ACCESS ID* en *AWS SECRET KEY*)
Als een cursist zelf al een AWS Access Key heeft voor AWS kan/mag deze gebruikt worden.
Het is verstandig om de AWS Access Key niet in de bestanden op te nemen, maar als variables.

```
export AWS AWS_ACCESS_KEY_ID=AKIA...
export AWS_SECRET_ACCESS_KEY=...
```

De workshop maakt gebruik van de **eu-west-1** regio en **t2.nano** EC2 type.
Verder is er toegang nodig tot de volgende diensten/rechten:

- AWS Security Groups
- AWS Elastic Loadbalancing (ELB)

Verder is de applicatie `terraform` nodig. Deze is te downloaden van [Terraform](https://releases.hashicorp.com/terraform) website.

Voor deze workshop is Terraform versie 0.12 of hoger nodig. Vanaf 0.12 is er gebruik gemaakt van HCL versie 2. Oude syntax (pre 0.12) werkt grotendeels prima, los van aantal bijzonderheden. (zie [Terraform 0.12 upgrade](https://www.terraform.io/upgrade-guides/0-12.html))

## Voorbereiding

Voor de workshop is een extra AWS account nodig met gelimiteerde toegang tot AWS. Deze stap is alleen nodig voor cursisten die zelf geen AWS account hebben.

In de `preparation` folder vindt je Terraform configuratie bestanden ter voorbereiding voor de workshop. Om een AWS user en AWS Access Key aan te maken voor de cursisten is een AWS account met AWS access key nodig.

```
cd preparation
export TF_VAR_account_id=... # De account ID waar een nieuwe account wordt
                             # aangemaakt.
terraform init
terraform plan
terraform apply
```

## Students verwachtingen

Van een student wordt ervan uitgegaan dat de volgende concepten begrijpt:

- OS command-line interactie
- Publieke cloud oplossingen (bij voorkeur AWS)
- Git
- Text Editor

## Opdrachten

- 101: Deploy een instance
- 102: Toegang tot instance
- 103: Provision de instance
- 104: Meerdere instances
- 105: Loadbalancing
