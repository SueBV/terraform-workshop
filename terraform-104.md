# 104 - Multiply

## Lab

### Opdracht 3 - Maak meerdere instances

Er is nu een instance aangemaakt met een webservice, maar 1 instance is niet voldoende. Dus, maak meerdere instances aan met dezelfde instellingen. (v.b. 3 of 5)

### Vragen

- Welk probleem krijgen we al meer dan 1 instance aanmaken?
- Hoe kunnen we dit hetbeste oplossen?
