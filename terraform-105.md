# 105 - Loadbalancing

## Lab

In deze lab gaan we instances die zijn aangemaakt koppelen aan een Loadbalancer, zodat de load verdeeld wordt over de aangemaakte webservers.

### Opdracht 5: Loadbalance de webservices

Maak een Elastic Loadbalancer (ELB Classic) aan en zorg dat het verkeer verdeeld wordt over de webservers.
Hou rekening mee dat voor een ELB eigen security regels toegevoegd moet worden.

Probeer niet zelf de Availability Zones (AZ) te defineren, maar op te vragen bij AWS.
Er zijn veel instellingen mogelijk binnen de ELB resource type, maar voor deze opdracht zijn de volgende het belangrijkst:

- listener
- health_check
- instances
- availability_zone

Als extra opdracht zorg dat alle relevante informatie te zien is na elke uitvoering van je configuratie.
