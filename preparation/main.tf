#
# Providers stanza
provider "aws" {
  region = "eu-west-1"
}

#
# Variables stanza
variable "account_id" {
  type        = string
  description = "AWS Account ID"
}

variable "pgp_key" {
  type        = string
  description = "Either PGP/GPG public key (base64 encoded) or the Keybase username prefixed with 'keybase:'."
}

#
# Data stanza
data "template_file" "iam_policy" {
  template = file("tmpl/iam_policy.tmpl")

  vars = {
    account_id = var.account_id
  }

}

#
# Resources stanza
resource "aws_iam_user" "tf" {
  name = "tf101"
}

resource "aws_iam_access_key" "tf" {
  user    = aws_iam_user.tf.name
  pgp_key = var.pgp_key
}

resource "aws_iam_user_policy" "tf-101" {
  name   = "tf-101"
  user   = aws_iam_user.tf.name
  policy = data.template_file.iam_policy.rendered
}

#
# Outputs stanza
output "tf_iam_access" {
  value = aws_iam_access_key.tf.id
}

output "tf_iam_secret" {
  value = aws_iam_access_key.tf.encrypted_secret
}
